# WhisperFusionApp

此項目是使用 [Angular CLI](https://github.com/angular/angular-cli) 版本 16.2.9 生成的。

## 開發伺服器

執行 `ng serve` 啟動開發伺服器。導航至 `http://localhost:4200/`。如果你更改了任何原始檔案，應用程序將自動重新載入。

## 使用 WhisperFusionApp 介面

### 啟動應用程序
1. 通過導航至 `http://localhost:4200/` 啟動應用程序。
2. 你將看到一個用戶友好的介面，設計用來控制音頻錄製和管理 WebSocket 連接。

### 錄製音頻
- 點擊「開始錄製」按鈕，通過設備的麥克風開始錄製音頻。
- 隨時可以通過按下「停止錄製」按鈕來停止並保存錄音。

### WebSocket 連接
- 使用提供的 WebSocket 介面連接到遠端音頻串流伺服器。
- 只需輸入 WebSocket URL 並點擊「連接」按鈕即可建立連接。

## 聲音串流
一旦連接，你錄製的音頻將實時串流到連接的伺服器，允許進行現場處理或儲存。

## 代碼腳手架

執行 `ng generate component component-name` 以生成新組件。你還可以使用 `ng generate directive|pipe|service|class|guard|interface|enum|module`。

## 建構

執行 `ng build` 以建構項目。建構後的檔案將存儲在 `dist/` 目錄下。

## 測試
### 單元測試
執行 `ng test` 通過 [Karma](https://karma-runner.github.io) 執行單元測試。

### 端對端測試
執行 `ng e2e` 通過你選擇的平台執行端對端測試。使用此命令前，你需要先添加實現端對端測試功能的包。

## 進一步幫助

若需關於 Angular CLI 的更多幫助，可執行 `ng help` 或查看 [Angular CLI 概覽及命令參考](https://angular.io/cli) 頁面。
