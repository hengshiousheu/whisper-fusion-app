import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { AppTranscriptionDisplayComponent } from './components/app-transcription-display/app-transcription-display.component';
import { WebSocketAudioControlComponent } from './components/web-socket-audio-control/web-socket-audio-control.component';

@NgModule({
  declarations: [
    AppComponent,
    AppTranscriptionDisplayComponent,
    WebSocketAudioControlComponent,
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
