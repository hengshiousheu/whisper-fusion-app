import { TestBed } from '@angular/core/testing';

import { WebAudioSocketService } from './web-audio-socket.service';

describe('WebAudioSocketService', () => {
  let service: WebAudioSocketService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(WebAudioSocketService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
