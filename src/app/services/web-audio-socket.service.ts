import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WebAudioSocketService {

    private websocket!: WebSocket; // 私有属性，用于存储 WebSocket 的实例

    private messageSubject = new Subject<any>(); // 使用 RxJS 的 Subject 来作为消息的观察者模式实现
    public messages$: Observable<any> = this.messageSubject.asObservable(); // 公开的 Observable，允许组件订阅消息
    private serverState = 0; // 用于跟踪服务器状态的内部变量

    private websocket_full_url!: string;

    constructor() { }

    // 连接到 WebSocket 服务器的方法，接受一个 URL 字符串参数
    public connect(url: string): void {
        // Handle transcription
        this.websocket_full_url = 'ws://' + url + '/transcription';

        this.websocket = new WebSocket(this.websocket_full_url);
        this.websocket.binaryType = "arraybuffer"; // 设置 WebSocket 的二进制消息类型为 ArrayBuffer

        // 设置 WebSocket 连接打开时的事件处理函数
        this.websocket.onopen = () => {
            console.log("Connected to server.");
            this.sendInitialData(); // 连接成功后发送初始化数据
        };

        // 设置 WebSocket 连接关闭时的事件处理函数
        this.websocket.onclose = (e) => {
            console.log("Connection closed (" + e.code + ").");
        };

        // 设置 WebSocket 接收到消息时的事件处理函数
        this.websocket.onmessage = (e) => {
            if (typeof e.data === 'string') {
                this.handleMessage(JSON.parse(e.data)); // 处理接收到的消息
            }
        };

    }

    // 发送 WebSocket 初始化数据的私有方法
    private sendInitialData() {
        const initData = {
            uid: this.generateUUID(), // 生成一个 UUID
            multilingual: true, // 是否支持多语言
            language: "zh", // 使用的语言
            task: "transcribe" // 任务类型
        };
        this.websocket.send(JSON.stringify(initData)); // 将初始化数据对象转换为 JSON 字符串并发送
    }

    // 生成 UUID 的方法
    private generateUUID(): string {
        return 'xxxx-4xxx-yxxx-xxxxxxx'.replace(/[xy]/g, function(c) {
            var r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }

    // 处理从 WebSocket 接收到的消息
    private handleMessage(data: any) {
        console.log("Message received:", data);
        // 检查是否包含特定的状态信息
        if ("message" in data) {
            if (data["message"] === "SERVER_READY") {
                this.serverState = 1; // 更新服务器状态
                this.messageSubject.next(data);
                console.log("Server is now ready.");
            }
            // 可以添加更多关于特定 message 类型的处理
        }

        // 处理转录数据
        if ("segments" in data) {
            if (Array.isArray(data.segments)) {
                // 如果 segments 是数组，推送给订阅者
                this.messageSubject.next(data);
            } else {
                // 如果 segments 存在但不是数组，记录错误
                console.error('Segments data is not an array:', data.segments);
            }
        } else {
            // 如果没有 segments 数据，可能是不含音频转录的其他类型消息
            console.log("Received non-segment data:", data);
        }

        // 检查其他可能的数据类型处理
        if ("error" in data) {
            console.error("Error received from server:", data.error);
        }
    }

    // 发送音频数据的方法
    public sendAudioData(audioData: ArrayBuffer): void {
        if (this.websocket && this.websocket.readyState === WebSocket.OPEN) {
        this.websocket.send(audioData);
        console.log('Audio data sent');
        }
    }

    // 检查 WebSocket 连接是否处于打开状态
    public isOpen(): boolean {
        return this.websocket && this.websocket.readyState === WebSocket.OPEN;
    }

    // 检查服务器是否已准备就绪，可以发送和接收数据
    public isServerReady(): boolean {
        return this.serverState === 1;
    }

    // 断开 WebSocket 连接的方法
    public disconnect(): void {
        if (this.websocket) {
            this.websocket.close(); // 关闭 WebSocket 连接
        }
    }  
}
