import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppTranscriptionDisplayComponent } from './app-transcription-display.component';

describe('AppTranscriptionDisplayComponent', () => {
  let component: AppTranscriptionDisplayComponent;
  let fixture: ComponentFixture<AppTranscriptionDisplayComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AppTranscriptionDisplayComponent]
    });
    fixture = TestBed.createComponent(AppTranscriptionDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
