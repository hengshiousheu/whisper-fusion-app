import { Component, OnInit } from '@angular/core';
import { WebAudioSocketService } from '../../services/web-audio-socket.service';

// 定义 Segment 接口用于描述音频转写片段的数据结构
interface Segment {
  start: number;  // 开始时间
  end: number;    // 结束时间
  text: string;   // 转写文本
}

@Component({
  selector: 'app-app-transcription-display',
  templateUrl: './app-transcription-display.component.html',
  styleUrls: ['./app-transcription-display.component.scss']
})
export class AppTranscriptionDisplayComponent implements OnInit {
  // 用于存储显示在界面上的转写消息数组
  displayMessages: Segment[] = [];

  constructor(
    private webAudioSocketService: WebAudioSocketService
  ) { }

  ngOnInit(): void {
    // 订阅 WebSocket 服务发来的消息
    this.webAudioSocketService.messages$.subscribe((data: any) => {
      // 当收到新的转写数据时，更新显示消息
      this.updateMessages(data.segments);
    });
  }

  // 更新显示消息的方法，用于处理新接收的转写片段
  private updateMessages(newSegments: Segment[]): void {

    // 检查 newSegments 是否是一个数组
    if (!newSegments || !Array.isArray(newSegments)) {
      console.error('Invalid or undefined segments data', newSegments);
      return;  // 如果不是数组或者为 undefined，直接返回
    }


    newSegments.forEach(segment => {
      // 查找是否已存在相同开始时间的消息
      const existingIndex = this.displayMessages.findIndex(msg => msg.start === segment.start);
      if (existingIndex > -1) {
        // 如果存在，替换现有消息
        this.displayMessages[existingIndex] = segment;
      } else {
        // 如果不存在，添加新消息
        // 检查是否达到消息显示的最大数量限制
        if (this.displayMessages.length >= 3) {
          this.displayMessages.shift(); // 移除最旧的消息以保持列表长度
        }
        this.displayMessages.push(segment); // 添加新的消息到列表
      }
    });
  }
}