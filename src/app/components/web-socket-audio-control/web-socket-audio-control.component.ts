import { Component } from '@angular/core';
import { WebAudioSocketService } from '../../services/web-audio-socket.service';

@Component({
  selector: 'app-web-socket-audio-control',
  templateUrl: './web-socket-audio-control.component.html',
  styleUrls: ['./web-socket-audio-control.component.scss']
})
export class WebSocketAudioControlComponent {

  public message!: string;
  private ip: string = '0.0.0.0';
  private port: string = '6006';
  private websocket_ip_port = this.ip + ":" + this.port

  private audioContext!: AudioContext;  // 私有属性，AudioContext 用于音频处理。
  private mediaStreamSource!: MediaStreamAudioSourceNode;  // 将媒体流（例如麦克风）连接到 Web Audio API。
  private audioWorkletNode!: AudioWorkletNode;  // AudioWorkletNode 用于处理音频流。

  constructor(private webAudioSocketService: WebAudioSocketService) {
    this.webAudioSocketService.messages$.subscribe(msg => {
      this.message = JSON.stringify(msg);
    });
  }

  connect(): void {
    this.webAudioSocketService.connect(this.websocket_ip_port);
  }

  disconnect(): void {
    this.webAudioSocketService.disconnect();
  }

  // 开始录音的方法，初始化音频设备和处理节点。
  async startRecording() {
    AudioContext = window.AudioContext;
    this.audioContext = new AudioContext({ latencyHint: 'interactive', sampleRate: 16000 });  // 创建 AudioContext，设置低延迟和采样率。

    try {
      console.log('this.audioContext =>', this.audioContext)
      if(this.audioContext){
        console.log('this.audioContext does exist')

        await this.audioContext.resume();
        const stream = await navigator.mediaDevices.getUserMedia({ audio: true });   // 请求用户麦克风权限并捕获音频流。

        if (!this.audioContext) return;
        console.log(this.audioContext?.state);

        await this.audioContext.audioWorklet.addModule('assets/audio-processor.js');  // 加载音频处理模块。

        const source = this.audioContext.createMediaStreamSource(stream); // 从获取的流创建音频源。
        this.audioWorkletNode = new AudioWorkletNode(this.audioContext, "audio-stream-processor"); // 创建音频处理工作节点。

        //配置工作节点消息事件，处理从 AudioWorklet 接收到的音频数据。
        this.audioWorkletNode.port.onmessage = (event) => {
          // 检查服务器是否准备好，并且 WebSocket 连接是打开的。
          if (this.webAudioSocketService.isServerReady() && this.webAudioSocketService.isOpen()) {
            const audioData = event.data;
            this.webAudioSocketService.sendAudioData(audioData.buffer);  // 发送音频数据通过 WebSocket。
          } else {
            console.warn('"server is not ready!!"' + "webSocketService:" +this.webAudioSocketService.isServerReady() + ", audioWebsocketService:"+ this.webAudioSocketService.isOpen());
          }
        };

        source.connect(this.audioWorkletNode);

      } else {
        console.log('this.audioContext QQQQQQ')
      }

    } catch (error) {
      console.error('Error starting audio recording:', error);  // 处理并记录开始录音过程中可能发生的错误。
    }
  }

  // 停止录音的方法，关闭音频上下文以释放资源。
  stopRecording() {
    if (this.audioContext) {
      this.audioContext.close();  // 关闭 AudioContext，确保所有音频处理停止并释放资源。
    }
  }

}
