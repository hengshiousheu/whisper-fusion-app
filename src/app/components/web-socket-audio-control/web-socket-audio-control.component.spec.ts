import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WebSocketAudioControlComponent } from './web-socket-audio-control.component';

describe('WebSocketAudioControlComponent', () => {
  let component: WebSocketAudioControlComponent;
  let fixture: ComponentFixture<WebSocketAudioControlComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [WebSocketAudioControlComponent]
    });
    fixture = TestBed.createComponent(WebSocketAudioControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
