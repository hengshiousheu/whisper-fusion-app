class AudioStreamProcessor extends AudioWorkletProcessor {
  constructor() {
    super();
    this.chunkSize = 4096; // 决定处理块的大小
    this.buffer = new Float32Array(this.chunkSize);
    this.bufferPointer = 0;
    this.energyThreshold = 0.0001; // 能量阈值，需根据实际情况调整
  }

  process(inputs, outputs, parameters) {
    const input = inputs[0];
    const output = outputs[0];
    let activeVoice = false; // 标记是否检测到有效声音
    let sumEnergy = 0; // 累积能量

    // 遍历所有输入样本
    for (let i = 0; i < input[0].length; i++) {
      const sample = input[0][i];
      this.buffer[this.bufferPointer] = sample;
      sumEnergy += sample * sample; // 计算能量

      if (++this.bufferPointer >= this.chunkSize) {
        // 判断平均能量是否超过阈值
        if (sumEnergy / this.chunkSize > this.energyThreshold) {
          console.log('sumEnergy =>', sumEnergy, 'this.chunkSize:', this.chunkSize )
          console.log('sumEnergy / this.chunkSize=>',sumEnergy / this.chunkSize)
          activeVoice = true;
        }
        // 重置能量计算和指针
        sumEnergy = 0;
        this.bufferPointer = 0;

        // 如果检测到有效声音，发送数据
        if (activeVoice) {
          this.port.postMessage(this.buffer);
          activeVoice = false; // 重置声音活动标记
        }
      }
    }

    // 将输入直接复制到输出，保持信号链
    for (let channel = 0; channel < output.length; channel++) {
      output[channel].set(input[channel]);
    }

    return true; // 保持处理器处于活跃状态
  }
}

registerProcessor("audio-stream-processor", AudioStreamProcessor);
