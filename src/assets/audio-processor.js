// AudioStreamProcessor 定義一個音頻工作處理器，用於實時處理和傳送音頻數據。
class AudioStreamProcessor extends AudioWorkletProcessor {

  // 構造函數初始化內部狀態。
  constructor() {
    super();

    // chunkSize 定義了每次處理和發送的音頻樣本的數量。// 此大小可以調整以控制數據發送的頻率和大小。
    // 時間長度(秒) = 緩衝區大小/樣本率
    this.chunkSize = 4096;
    // buffer 用於臨時存儲音頻數據，直到達到 chunkSize 為止。
    this.buffer = new Float32Array(this.chunkSize);
    // bufferPointer 跟踪當前緩衝區中音頻樣本的填充位置。
    this.bufferPointer = 0;
  }

  // process 方法是 AudioWorkletProcessor 的核心，每次音頻數據塊到來時被調用。
  process(inputs, outputs, parameters) {
    // 獲取輸入和輸出的音頻流。第一個輸入和輸出流通常用於單聲道音頻處理。
    const input = inputs[0];
    const output = outputs[0];

    // 計算可用的聲道數，以防有多個聲道的情況。
    let channelCount = Math.min(input.length, output.length);

    // 遍歷每個音頻樣本，並將其添加到 buffer 中。
    for (let i = 0; i < input[0].length; i++) {
      this.buffer[this.bufferPointer++] = input[0][i];

      // 當緩衝區滿時，通過 postMessage 將整個音頻數據發送出去。
      if (this.bufferPointer >= this.chunkSize) {
        this.port.postMessage(this.buffer);
        this.bufferPointer = 0;
      }
    }

    // 將處理過的音頻數據寫入輸出流，以供進一步的音頻處理或播放。
    for (let channel = 0; channel < channelCount; ++channel) {
      output[channel].set(input[channel]);
    }

    // 返回 true 保持處理器處於活躍狀態，準備處理下一批音頻數據。
    return true;
  }
}

// 在音頻工作處理器的全局範圍內註冊處理器，使其可在 AudioContext 中被實例化和使用。
registerProcessor("audio-stream-processor", AudioStreamProcessor);
